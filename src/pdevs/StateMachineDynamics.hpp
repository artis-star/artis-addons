/**
 * @file pdevs/StateMachineDynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_STAR_ADDONS_STATE_MACHINE_DYNAMICS_HPP
#define ARTIS_STAR_ADDONS_STATE_MACHINE_DYNAMICS_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

namespace artis::addons::pdevs {

template<class Time, class Model, class Parameters, class RootStateMachine>
class StateMachineDynamics : public artis::pdevs::Dynamics<Time, Model, Parameters> {
public:
  StateMachineDynamics(const std::string &name, const artis::pdevs::Context<Time, Model, Parameters> &context) :
    artis::pdevs::Dynamics<Time, Model, Parameters>(name, context), _root() {
    _root.build(context.parameters());
  }

  virtual ~StateMachineDynamics() = default;

  void
  dconf(const typename Time::type &t, const typename Time::type &e, const common::event::Bag <Time> &bag) override {
    dint(t);
    dext(t, e, bag);
  }

  void dint(const typename Time::type &t) override {
    _root.transition(t);
  }

  void dext(const typename Time::type & /* t */, const typename Time::type & /* e */,
            const common::event::Bag <Time> & /* bag*/) override {}

  void start(const typename Time::type &t) override {
    _root.start(t);
  }

  typename Time::type ta(const typename Time::type &t) const override {
    return _root.ta(t);
  }

  common::event::Bag <Time> lambda(const typename Time::type & /* t */) const override { return {}; }

  artis::common::event::Value
  observe(const typename Time::type & /* t */, unsigned int /* index */) const override { return {}; }

protected:
  const RootStateMachine &root() const { return _root; }

  RootStateMachine &root() { return _root; }

private:
  RootStateMachine _root;
};

}

#endif //ARTIS_STAR_ADDONS_STATE_MACHINE_DYNAMICS_HPP
