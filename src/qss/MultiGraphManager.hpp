/**
 * @file qss/MultiGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_STAR_ADDONS_QSS_MULTI_GRAPH_MANAGER
#define ARTIS_STAR_ADDONS_QSS_MULTI_GRAPH_MANAGER

#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/qss/MultiDerivative.hpp>
#include <artis-star/kernel/qss/Integrator.hpp>
#include <artis-star/kernel/qss/Quantifier.hpp>

namespace artis::addons::qss {

template<class DerivativeParameters>
struct MultiQSSParameters {
  std::vector <artis::qss::IntegratorParameters> integrators;
  std::vector <artis::qss::QuantifierParameters> quantifiers;
  DerivativeParameters derivative;
};

template<class Time, class Derivative, class DerivativeParameters = artis::common::NoParameters>
class MultiGraphManager :
  public artis::pdevs::GraphManager<Time, MultiQSSParameters<DerivativeParameters>, artis::common::NoParameters> {
public:
  struct submodel {
    enum values {
      S_Derivative = 0, S_Integrator = 1, S_Quantifier = 1000
    };
  };

  struct input {
    enum values {
      RESET
    };
  };

  MultiGraphManager(common::Coordinator <Time> *coordinator,
                    const MultiQSSParameters<DerivativeParameters> &parameters,
                    const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<Time, MultiQSSParameters<DerivativeParameters>, artis::common::NoParameters>(
      coordinator, parameters, graph_parameters),
    _derivative("d", parameters.derivative) {
    this->add_child(submodel::S_Derivative, &_derivative);
    coordinator->input_port({input::RESET, "reset"});
    this->in({coordinator, input::RESET})
      >> this->in({&_derivative, Derivative::input::RESET});
    for (unsigned int index = 0; index < _derivative.dynamics().variable_number(); ++index) {
      _integrators.push_back(new IntegratorSimulator("i_" + std::to_string(index),
                                                     parameters.integrators[index]));
      _quantifiers.push_back(new QuantifierSimulator("q_" + std::to_string(index),
                                                     parameters.quantifiers[index]));
      this->add_child(submodel::S_Integrator + index, _integrators.back());
      this->add_child(submodel::S_Quantifier + index, _quantifiers.back());
      coordinator->output_port({index, "out_" + std::to_string(index)});
      this->in({coordinator, input::RESET})
        >> this->in({_integrators.back(), artis::qss::Integrator<Time>::input::RESET});
      this->in({coordinator, input::RESET})
        >> this->in({_quantifiers.back(), artis::qss::Quantifier<Time>::input::RESET});

      this->out({&_derivative, index})
        >> this->in({_integrators.back(), artis::qss::Integrator<Time>::input::X_DOT});
      this->out({_integrators.back(), artis::qss::Integrator<Time>::output::OUT})
        >> this->in({_quantifiers.back(), artis::qss::Quantifier<Time>::input::IN});
      this->out({_quantifiers.back(), artis::qss::Quantifier<Time>::output::OUT})
        >> this->in({_integrators.back(), artis::qss::Integrator<Time>::input::QUANTA});
      this->out({_integrators.back(), artis::qss::Integrator<Time>::output::OUT})
        >> this->out({coordinator, index});
      this->out({_integrators.back(), artis::qss::Integrator<Time>::output::OUT})
        >> this->in({&_derivative, Derivative::input::INTERNAL + index});
    }
  }

  ~MultiGraphManager() override {
    std::for_each(_integrators.begin(), _integrators.end(), std::default_delete<IntegratorSimulator>());
    std::for_each(_quantifiers.begin(), _quantifiers.end(), std::default_delete<QuantifierSimulator>());
  }

  artis::pdevs::Simulator<Time, Derivative, DerivativeParameters> *derivative() { return &_derivative; }

private:
  typedef artis::pdevs::Simulator<Time, artis::qss::Integrator<Time>, artis::qss::IntegratorParameters> IntegratorSimulator;
  typedef std::vector<IntegratorSimulator *> IntegratorSimulators;
  typedef artis::pdevs::Simulator<Time, artis::qss::Quantifier<Time>, artis::qss::QuantifierParameters> QuantifierSimulator;
  typedef std::vector<QuantifierSimulator *> QuantifierSimulators;

  artis::pdevs::Simulator<Time, Derivative, DerivativeParameters> _derivative;
  IntegratorSimulators _integrators;
  QuantifierSimulators _quantifiers;
};

}

#endif
